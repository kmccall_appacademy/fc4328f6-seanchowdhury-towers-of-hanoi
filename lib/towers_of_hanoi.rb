# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_accessor :towers

  def initialize(num = 3)
    @towers = [[],[],[]]
    @towers[0] = (1..num).to_a.reverse
    @slength = num
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    return false if from_tower > 2 || to_tower > 2
    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    return false if @towers[from_tower].last > @towers[to_tower].last
    true
  end

  def won?
    return true if @towers[0].empty? && @towers[1].empty?
    return true if @towers[0].empty? && @towers[2].empty?
    false
  end

  def interact
    puts "pick up a disc"
    @from_tower = gets.chomp.to_i
    puts "place it somewhere"
    @to_tower = gets.chomp.to_i
  end

  def play
    render(@towers)
    until won?
      interact
      until valid_move?(@from_tower, @to_tower)
        puts "invalid move"
        render(@towers)
        interact
      end
      move(@from_tower, @to_tower)
      render(@towers)
    end
    puts "wow can't believe you did it"
  end

  def render(array)
    arr = array.dup
    arr.map!(&:reverse)
    arr.each do |el|
      el.unshift(0) until el.length == @slength if el.length < @slength
    end
    arr = arr[0].zip(*arr[1..-1])
    arr.each { |el| puts el.to_s }
  end

end
